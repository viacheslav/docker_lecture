1. Go to /app/src/main/docker
    ```
        
    ```
2. Start image building

    ```
        docker build .
    ``` 
    
3. Build fails. Try to include /target folder - update Dockerfile  
    Get error:
    
    ```
         ADD failed: Forbidden path outside the build context: ../../../build/libs/app.jar
    ```
 
4. Build image with Gradle plugin

    ```
        ./gradlew buildDocker
    ```

5. Check new image in list
    ```
    ```
6. Run container

    ```
        docker run -d -p 80:8080 <image>
    ``` 
     
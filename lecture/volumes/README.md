## Steps to show

1. Create a volume:
    ```
        docker volume create vol
    ```

2. Show list of volumes
    ```
        docker volume ls
    ```
    
3. Inspect volume
    ```
        docker volume inspect vol
    ```
    
4. Run docker image with volume
    ```
        docker run -it -v vol:/app -p 80:80 nginx:latest
    ```   
    
5. Get into container and check volume
    ```
        docker exec -i -t <container> /bin/bash
        cd /app
        touch app.py
    ```
    
6. Run new container with the same volume
    ```
        docker run -it -v vol:/app -p 8080:80 nginx:latest
        cd /app
        ls
    ```

7. Stop containers, delete volumes
    ```
        docker stop <container>
        docker volume rm  <volume> OR docker volume prune
    ``` 
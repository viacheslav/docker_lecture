1. Copy app.jar to docker folder
2. Run
    ```
        docker-compose up
    ``` 
3. Check results of 
    ```
        docker ps
    ```
4. Run from directory with docker-compose.yml:
    ```
        docker-compose ps
    ``` 
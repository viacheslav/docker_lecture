1. Create 2 Virtualbox VMs with docker-machine
    
    ```
        docker-machine create --driver virtualbox vm1
    ```
   
2. Init swarm cluster

    ```
        docker-machine ssh vm1 "docker swarm init --advertise-addr 192.168.99.100:2377"
    ```
    
3. Add second VM as worker

    ```
        docker-machine ssh vm2 "docker swarm join --token SWMTKN-1-5gwtlhq7w8593jdlj9veydomqejwb58oi9p84ydalw20m21h3t-6rmjd2gx779lomdffkjxjx350 192.168.99.100:2377"
    ```
    
4. SSH into VM1

    ```
        docker-machine ssh vm1
    ```
    
5. See cluster state

    ```
        docker node ls
    ```
    
6. Update docker-compose.yml: remove custom build

7. Copy docker-compose.yml to VM1

    ```
        docker-machine scp docker-compose.yml vm1:~
    ```
    
8. SSH into VM1 and deploy stack

    ```
        docker-machine ssh vm1
        docker stack deploy -c docker-compose.yml example
    ```
    
9. 
    ```
        docker stack ls AND docker stack ps <name>
    ```